<?php
class Frog {
    public $name;
    public $legs = 4;
    public $cold_blooded = "no";

    public function jump(){
        echo "Jump: hop hop";
    }

    public function __construct($string){
        $this->name = $string; 
    }
}; 

// "hop hop"
?>