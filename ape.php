<?php
class Ape {
    public $name;
    public $legs = 2;
    public $cold_blooded = "no";

    public function yell(){
        echo "Yell: Auooo";
    }

    public function __construct($string){
        $this->name = $string;  
    }
};
// "Auooo"
?>