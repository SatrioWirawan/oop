<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP - OOP</title>
</head>
<body>
    <?php
    echo "<h3>PHP - OOP</h3>";

    require_once("animal.php");
    require_once("ape.php");
    require_once("frog.php");

    $sheep = new Animal("shaun");
    $sheep->name;

    echo "Name = $sheep->name <br>"; // "shaun"
    echo "Legs = $sheep->legs <br>"; // 4
    echo "Cold Blooded = $sheep->cold_blooded<br>"; // "no"
    echo "<br>";

    $kodok = new Frog("buduk");

    echo "Name = $kodok->name <br>";
    echo "Legs = $kodok->legs <br>";
    echo "Cold Blooded = $kodok->cold_blooded<br>";
    $kodok->jump();
    echo "<br>";
    echo "<br>";

    $sungokong = new Ape("kera sakti");

    echo "Name = $sungokong->name <br>";
    echo "Legs = $sungokong->legs <br>";
    echo "Cold Blooded = $sungokong->cold_blooded<br>";
    $sungokong->yell() 
    ?>
</body>
</html>